const mongoose= require('mongoose');
const CONFIG = require('./config');

module.exports = {
    
    connection:null,
    connect:()=>{
        
        return mongoose.connect(CONFIG.DB, {useUnifiedTopology: true,useNewUrlParser:true}).then(connection =>{
            this.connection=connection;
            console.log('Conexión exitosa');
        }).catch(err => console.log(err));

    }
}
    

