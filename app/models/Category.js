const mongoose = require ("mongoose");

const CategorySchema = new mongoose.Schema({

    tipo:String,
    nombre:String,
    descripcion:String
});

const Category = mongoose.model("Category", CategorySchema);
module.exports = Category;