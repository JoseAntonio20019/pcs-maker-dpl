const mongoose = require("mongoose");

const ComputerSchema = new mongoose.Schema({
    
    tipo:String,
    procesador:String,
    placabase:String,
    ram:String,
    disco_duro_principal: String,
    disco_duro_secundario: String,
    refrigeracion:String,
    tarjeta_grafica:String,
    fuente_alimentacion:String,
    caja:String
    
});

const Computer = mongoose.model("Computer", ComputerSchema);
module.exports = Computer;
