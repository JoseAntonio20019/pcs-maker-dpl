module.exports={

    header:
        `<!DOCTYPE html>
        <html lang="en">
        <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <nav class="navbar navbar-expand-lg navbar-light bg-dark p-3">
        <div class="container-fluid">
          <a class="navbar-brand text-white" href="/index">PC'S Maker: El creador de PC'S definitivos</a>
        </div>
        </nav>`,

        footer:
        `<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <footer class="text-center text-white bg-secondary fixed-bottom bg-dark p-4">
         <p>Jose Antonio Martínez Crespo © 2021 Copyright</p>
         </footer>
         </body>
         </html>
         `,


        categoryView:
        `<div class="container mt-5">
        <div class="row row-cols gap-3">
            <div class="row gap-3">
        <h2 class="text-center">Categorías</h2>`,

        computerView:`<div class="container mt-5">
        <div class="row row-cols gap-3">
            <div class="row gap-3">
        <h2 class="text-center">Ordenadores</h2>`


    
}