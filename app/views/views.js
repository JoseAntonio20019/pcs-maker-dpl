const parts = require("./parts");

function computerView() {
  let content='';
  content += `
    <div class="card" style="width: 18rem;">
    <div class="card-body">
    <h4 class="card-title">Tipo:<a class="text-decoration-none text-black" href="/computer/${computer.tipo}">${computer.tipo}</a></h4>
    <p class="card-text">Procesador:${computer.procesador}</p>
    <p class="card-text">RAM:${computer.ram}</p>
    <p class="card-text">Placa Base:${computer.placabase}</p>
    <p class="card-text">Disco Duro 1:${computer.disco_duro_principal}</p>
    <p class="card-text">Disco Duro 2:${computer.disco_duro_secundario}</p> 
    <p class="card-text">Refrigeración:${computer.refrigeracion}</p>                  
    <p class="card-text">Tarjeta Gráfica:${computer.tarjeta_grafica}</p>
    <p class="card-text">Fuente Alimentación:${computer.fuente_alimentacion}</p>
    <p class="card-text">Caja:${computer.caja}</p>
    <div class="d-flex gap-3">
    <p class="card-text"><a class="btn btn-success" href="/computer/editar/${computer.id}">Editar</a></p>
    <p class="card-text"><a class="btn btn-danger" href="/computer/borrar/${computer.id}">Borrar</a></p>
    </div>
    </div>
    </div>
    `;
content +=parts.footer;

  return content;
}

function computerNew(docs) {
  let content=parts.header;
  content += `<title>Ordenador Nuevo</title>
        </head>

        <body>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card card-body bg-light p-6 mt-5 mb-5">
                        <form method="post" action="/computer/crear">
                            <h4>Bienvenido al creador de tu PC:</h4>
                            <h6>Tipo de PC:</h6>
                            `;
                        
                                    for(category of docs) {
                                  content +=
                                  `
                                  <input type="radio" id="${category.tipo}" name="tipo" value="${category.tipo}">
                                  <label for="${category.tipo}">${category.tipo}</label>
                                  <br>
                                  `;
                              }
                           content += `
                            <h6>Procesador:</h6>
                            <input type="text" name="procesador" class="form-control" placeholder="Ej: AMD Ryzen">
                            <h6>Placa Base:</h6>
                            <input type="text" name="placabase" class="form-control" placeholder="Ej: Aours Z-690 Max">
                            <h6>Memoria RAM:</h6>
                            <input type="text" name="ram" class="form-control" placeholder="Ej: Corsair Vengeance CL16 2x8 3200 mhz">
                            <h6>Disco Duro Principal</h6>
                            <input type="text" name="disco_duro_principal" class="form-control" placeholder="Ej: Samsung EVO 1TB M.2">
                            <h6>Disco Duro Secundario</h6>
                            <input type="text" name="disco_duro_secundario" class="form-control" placeholder="Ej: Sand Disk 500gb SATA 3">
                            <h6>Refrigeración:</h6>
                            <input type="text" name="refrigeracion" class="form-control" placeholder="Ej: NZXT Kraken">
                            <h6>Tarjeta Gráfica:</h6>
                            <input type="text" name="tarjeta_grafica" class="form-control" placeholder="Ej: Nvidia Geforce RTX 3060">
                            <h6>Fuente de Alimentación:</h6>
                            <input type="text" name="fuente_alimentacion" class="form-control" placeholder="Ej: Corsair CX650M 650W 80 Plus Bronze">
                            <h6>Caja:</h6>
                            <input type="text" name="caja" class="form-control" placeholder="Ej: Corsair 5000D ATX">
                            <br>
                            <br>
                            <input class="btn btn-primary" type="submit" value="Enviar">
                            </form>
                    </div>
                </div>
            </div>
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <footer class="text-center text-white bg-secondary  bg-dark p-4">
         <p>Jose Antonio Martínez Crespo © 2021 Copyright</p>
         </footer>
         </body>
         </html>`;


  return content;
}

function computerEdit(data) {
  let content=parts.header;
  content += `
    <body>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light p-6 mt-5 mb-5">
            <form method="post" action="/computer/update">
                <h4>Actualizar tu PC</h4>
                <input type="hidden" name="id"  class="form-control" value="${data._id}"/>
                <h6>Procesador:</h6>
                <input type="text" name="procesador" id="procesador" class="form-control" value="${data.procesador}"/>
                <h6>Placabase:</h6>
                <input type="text" name="placabase" id="placabase" class="form-control" value="${data.placabase}"/>
                <h6>Ram:</h6>
                <input type="text" name="ram" id="ram" class="form-control" value="${data.ram}"/>
                <h6>Disco Duro Principal:</h6>
                <input type="text" name="disco_duro_principal" id="disco_duro_principal" class="form-control" value="${data.disco_duro_principal}"/>
                <h6>Disco Duro Secundario:</h6>
                <input type="text" name="disco_duro_secundario" class="form-control" id="disco_duro_secundario" value="${data.disco_duro_secundario}"/>
                <h6>Refrigeración:</h6>
                <input type="text" name="refrigeracion" id="refrigeracion" class="form-control" value="${data.refrigeracion}"/>
                <h6>Tarjeta Gráfica:</h6>
                <input type="text" name="tarjeta_grafica" id="tarjeta_grafica" class="form-control" value="${data.tarjeta_grafica}"/>
                <h6>Fuente Alimentación:</h6>
                <input type="text" name="fuente_alimentacion" id="fuente_alimentacion"  class="form-control"value="${data.fuente_alimentacion}"/>
                <h6>Ram:</h6>
                <input type="text" name="caja" id="caja" class="form-control" value="${data.caja}"/>
                <input class="btn btn-primary mt-3" type="submit" value="Actualizar">
            </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
     </body>
     <div class="text-center text-white bg-secondary bg-dark p-4 ">
     <p>Jose Antonio Martínez Crespo © 2021 Copyright</p>
     </div>
     </html>`;

    content +=parts.footer;
  return content;
}

function categoryView() {

  let content='';
  content += `
  <div class="card mb-6 mt-6 w-25">
  <div class="card-body">
    <h4>${category.tipo}</h4>
    <p>Nombre Categoría: ${category.nombre}</p>
    <p>Descripción: ${category.descripcion}</p>
    <div class="d-flex gap-3">
    <p><a class="btn btn-success" href="/category/edit/${category.id}">Editar</a></p>
    <p><a class="btn btn-danger" href="/category/borrar/${category.id}">Borrar</a></p>
    </div>
    </div>
    </div>`;
    content +=parts.footer;
  return content;
}

function categoryNew() {
  let content = parts.header;

  content += `<title>Categoría Nueva</title>
        </head>
        <body>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card card-body bg-light p-6 mt-5 mb-5">
                        <form method="post" action="/category/crear">
                        <h4>Tipo:</h4>
                        <input type="text" name="tipo" class="form-control" placeholder="PC para Ofimática">
                        <h6>Nombre:</h6>
                        <input type="text" name="nombre" class="form-control" placeholder="PC para Ofimática" >
                        <h6>Descripción:</h6>
                        <input type="text" name="descripcion" class="form-control" placeholder="Este clase de PC está enfocado a ...">
                        <input class="btn btn-primary mt-3" type="submit" value="Crear">
                        </form>
                        </div>
                    </div>
                </div>`;
                content +=parts.footer;

  return content;
}

function categoryEdit(data) {
  let content = parts.header;

  content += `<title>Editar Categoría</title>
        </head>
        <body>
        <h3 class="text-center mt-5">Editar Categoría</h3>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card card-body bg-light p-6 mt-5 mb-5">
                        <form method="post" action="/category/update">
                        <input type="hidden" name="id"  class="form-control" value="${data._id}"/>
                        <h6>Tipo:</h6>
                        <input type="text" name="tipo" class="form-control" placeholder="PC para Ofimática" value="${data.tipo}" >
                        <h6>Nombre:</h6>
                        <input type="text" name="nombre" class="form-control" placeholder="PCS para ofimática" value="${data.nombre}">
                        <h6>Descripción:</h6>
                        <input type="text" name="descripcion" class="form-control" placeholder="Este clase de PC está enfocado a ..." value="${data.descripcion}">
                        <input class="btn btn-primary mt-3" type="submit" value="Editar">
                        </form>
                        </div>
                    </div>
                </div>`;

                content +=parts.footer;

  return content;
}

module.exports = {
  computerView,
  computerNew,
  computerEdit,
  categoryView,
  categoryNew,
  categoryEdit,
};
