const express = require("express");
const app = express();
const path = require("path");
const Computer = require("./routes/computer");
const Category = require("./routes/category");
const bodyParser = require("body-parser");

app.use(express.static("html"));
app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname, "/html/login.html"));
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.post("/", (req, res) => {
  if (req.body.username == "Jose" && req.body.password == "1234") {
    res.redirect("/index");
  } else {
    res.send("El usuario introducido no es correcto");
  }
});

app.use("/index", express.static(__dirname + "/html"));

app.use("/computer", Computer);
app.use("/category", Category);

module.exports = app;
