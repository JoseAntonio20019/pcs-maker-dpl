const Computer = require("../models/Computer");
const Category = require("../models/Category");
const path = require("path");
const view = require("../views/views");
const parts = require("../views/parts");

/*/////////////////////////////////////////////
                FIND
/////////////////////////////////////////////*/

function find(req, res) {

  let filtro = {};

  if (req.params.tipo) {
    filtro = { tipo: req.params.tipo};
  }
  Computer.find(filtro, (err, docs) => {
    if (err) {
      console.log(err);
      console.log("Error buscando los PCS");
    } else {
      let content= parts.header;
      content +=parts.computerView;
      for(computer of docs) {
        content += view.computerView();
      }

      content += `<h5><a class="btn btn-primary" role="button" href="/computer/crear">Añadir PC</a></h5>
        </body></html>`;
      res.send(content);
    }
  }).collation({ locale: "es", strength: 2 });
}

/*/////////////////////////////////////////////
              CREATE
/////////////////////////////////////////////*/
function create(req, res) {
  const nuevocomputer = new Computer({
    tipo: req.body.tipo,
    procesador: req.body.procesador,
    placabase: req.body.placabase,
    ram: req.body.ram,
    disco_duro_principal: req.body.disco_duro_principal,
    disco_duro_secundario: req.body.disco_duro_secundario,
    refrigeracion: req.body.refrigeracion,
    tarjeta_grafica: req.body.tarjeta_grafica,
    fuente_alimentacion: req.body.fuente_alimentacion,
    caja: req.body.caja,
  });
  nuevocomputer.save(function (err) {
    if (err) {
      res.send("Error creando el PC");
    } else {
      res.redirect("/computer");
      console.log(nuevocomputer);
    }
  });
}

/*/////////////////////////////////////////////
                DELETE
/////////////////////////////////////////////*/
function deleted(req, res) {
  Computer.deleteOne({ _id: req.params.id }, (err, result) => {
    if (err) {
      res.send(console.log("Error borrando el PC"));
    } else {
      res.redirect("/computer");
    }
  });

}

/*/////////////////////////////////////////////
              EDIT
/////////////////////////////////////////////*/
function edit(req, res) {
  Computer.findOne({ _id: req.params.id }, (err, data) => {
    if (err) {
      console.log(err);
      res.send("Error editando el PC");
    } else {
      res.send(view.computerEdit(data));
      console.log("PC editado con éxito");
    }
  });
}

/*/////////////////////////////////////////////
                UPDATE
/////////////////////////////////////////////*/
function update(req, res) {
  Computer.findOneAndUpdate(
    { _id: req.body.id },
    {
      tipo: req.body.tipo,
      procesador: req.body.procesador,
      placabase: req.body.placabase,
      ram: req.body.ram,
      disco_duro_principal: req.body.disco_duro_principal,
      disco_duro_secundario: req.body.disco_duro_secundario,
      refrigeracion: req.body.refrigeracion,
      tarjeta_grafica: req.body.tarjeta_grafica,
      fuente_alimentacion: req.body.fuente_alimentacion,
      caja: req.body.caja,
    },
    (err, data) => {
      if (err) {
        //No se ha podido actualizar

        res.send("ERROR");
      } else {
        res.redirect("/computer");
      }
    }
  );
}

function index(req, res) {
  res.sendFile(path.resolve(__dirname, "../html/index.html"));
}

function redirect(req, res){

  Category.find((err, docs) => {
                              
                          
         let content='';
          content +=view.computerNew(docs);   
    
    res.send(content);
   
})};



module.exports = {
  find,
  create,
  edit,
  update,
  index,
  redirect,
  deleted,
};
