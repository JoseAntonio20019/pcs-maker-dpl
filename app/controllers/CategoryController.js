const Category = require("../models/Category");
const path= require('path');
const view = require('../views/views');
const parts = require('../views/parts');


/*/////////////////////////////////////////////
                FIND
/////////////////////////////////////////////*/ 

function find(req,res){
    let filtro = {};

    if(req.params.tipo){

        filtro = {tipo: req.params.tipo}
    }

    Category.find(filtro,(err,docs)=>{

        if(err){
            console.log(err);
            console.log("Error buscando la categoría");

        }else{

            let content=parts.header;
            content +=parts.categoryView
            for(category of docs){

                content+= view.categoryView();
            }

            content +=`<h5><a class="btn btn-primary" role="button" href="/category/crear">Añadir categoría</a></h5>`;
            
            res.send(content);

        }

    }).collation({locale: "es" , strength : 2});
}


/*/////////////////////////////////////////////
              CREATE
/////////////////////////////////////////////*/ 

function create(req,res){

    const nuevaCategory= new Category({

        tipo:req.body.tipo,
        nombre:req.body.nombre,
        descripcion:req.body.descripcion

    });
    nuevaCategory.save(function (err){
        if(err) {

            res.send("Error creando la categoría");
        }else{
            console.log(nuevaCategory);
            res.redirect('/category');
        }
    });


}

/*/////////////////////////////////////////////
                DELETE
/////////////////////////////////////////////*/ 

function deleted(req,res){
    Category.deleteOne({ id:req.params.id}, (err, result) => {
        if(err){

            res.send(console.log("Error borrando la categoría"));
        }else{

            res.redirect('/category');
            console.log(result);
        }


    });
    console.log(res);

}

/*/////////////////////////////////////////////
              EDIT
/////////////////////////////////////////////*/ 

function edit(req,res) {
    Category.findOne(
        { _id: req.params.id },(err,data) => {

            if(err){

                console.log(err);
                res.send("Error editando la categoría");
            }else{

                res.send(view.categoryEdit(data));
                console.log("Categoría editada con éxito");
            }

        }

    );
}

/*/////////////////////////////////////////////
                UPDATE
/////////////////////////////////////////////*/ 

function update(req,res){

    Category.findOneAndUpdate({_id: req.body.id},{

        'tipo':req.body.tipo,
        'nombre':req.body.nombre,
        'descripcion':req.body.descripcion

    }, (err,data) =>{

        if(err){

            res.send("Error actualizando la Categoría");
        }else{

            res.redirect('/category');
        }


    });

}

function index(req,res){

    res.sendFile(path.resolve(__dirname, "../html/index.html"));

}

function redirect(req,res){

    res.send(view.categoryNew());
    
};

    



module.exports = {

    find,
    create,
    edit,
    update,
    index,
    redirect,
    deleted,



}

