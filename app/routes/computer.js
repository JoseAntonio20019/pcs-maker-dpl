const express = require("express");
const ComputerController = require("../controllers/ComputerController");

const router = express.Router();

router.get('/', ComputerController.find)
    .get('/crear', ComputerController.redirect)
    .get('/borrar/:id', ComputerController.deleted)
    .get('/editar/:id', ComputerController.edit)
    .post('/update', ComputerController.update)
    .post('/crear',ComputerController.create)
    .get('/:tipo?',ComputerController.find)


    module.exports= router;

    

