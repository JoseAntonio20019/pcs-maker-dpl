const express = require("express");
const CategoryController = require("../controllers/CategoryController");

const router =express.Router();

router.get('/',CategoryController.find)
      .get('/crear',CategoryController.redirect)
      .get('/:tipo?',CategoryController.find)
      .get('/edit/:id',CategoryController.edit)
      .get('/borrar/:id',CategoryController.deleted)
      .post('/update',CategoryController.update)
      .post('/crear',CategoryController.create)


module.exports = router;